import nltk.data
import pickle

# `python train_classifier.py --algorithm NaiveBayes --instances files --fraction 1 --show-most-informative 10 --filename ../trained-classifier.pickle ../review_polarity/txt_sentoken/`

def scoreMovie(filename):
    review = open("MovieReviews/" + filename + ".txt")
    
    # rudamentary tokenizer
    words = review.read().split(' ')
    
    classifier = pickle.load(open("trained-classifier.pickle"))
    
    feats = dict([(word, True) for word in words])
    scores = classifier.prob_classify(feats)
    
    polarity = scores.max() # 'pos' or 'neg'
    score = scores.prob(polarity)
    if polarity == 'neg': score *= -1
    
    return score