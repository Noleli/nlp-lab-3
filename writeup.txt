Noah Liebman  
2012-05-28 -- Lab 3  
Linguistics 336

1. &nbsp;
    a. $S_{\mathcal{G}_1} = \begin{bmatrix} 0 & 1 & 0\\
                            \tfrac{1}{2} & 0 & \tfrac{1}{2}\\
                            1 & 0 & 0\end{bmatrix}$
    b. Using the power method, the eigenvector converges to $[0.4, 0.4, 0.2]$.
    c. A random walk converges to the proportions $[0.4, 0.4, 0.2]$ on $A$, $B$, and $C$, respectively, which makes sense because $A$ has two inlinks and can only go to $B$, so a random walk will spend the same amount of time on each of those, while $B$ can either go back to $A$ or go on to $C$, so it only spends half the time on $C$ as $B$.
    d. The Perron--Frobenius theorem can be applied in this case because the stochastic matrix $S_{\mathcal{G}_1}$ represents a network in which there is always a path between any two nodes; in other words, it contains no weak components and is therefore irreducible. It is a weak case of Perron--Frobenius, however, because each element is non-negative, but not positive.
2. &nbsp;
    a. For the three probability vectors I tried (and theoretically for all probability vectors), the eigenvector of $\mathcal{G}_2$ is [0.1538, 0.1538, 0.1538, 0.1538, 0.07692, 0.07692, 0.07692, 0.07692, 0.07692]. The three vectors I tried were a uniform distribution, a distribution that bumped $A$ down 0.1 and bumped $D$ up 0.1, and $[0, 0.4, 0, 0, 0.3, 0, 0, 0.3, 0]$.
    b. Looking at nodes in $\mathcal{G}_2$ that satisfy cases (i) and (ii),
        i. $\mathrm{\mathbf{v}}_A \leq \mathrm{\mathbf{v}}_B$
        ii. $\mathrm{\mathbf{v}}_A \geq \mathrm{\mathbf{v}}_B$
    c. &nbsp;
        i. The weight of $\mathrm{\mathbf{v}}_A$ is strictly less than $\mathrm{\mathbf{v}}_{B_n}$, while $\mathrm{\mathbf{v}}_{B_j} \lesseqgtr \mathrm{\mathbf{v}}_{B_k}$ as long as they're both less than $\mathrm{\mathbf{v}}_A$.
        ii. The weight of each $\mathrm{\mathbf{v}}_{A_n}$ is strictly less than $\mathrm{\mathbf{v}}_B$, while $\mathrm{\mathbf{v}}_{A_j} \lesseqgtr \mathrm{\mathbf{v}}_{A_k}$ as long as they're both less than $\mathrm{\mathbf{v}}_B$.
3. &nbsp;
    a. Yes. $\tfrac{1}{3}$ of the time is spent in each of $A$ and $B$, and $\tfrac{1}{6}$ of the time is spent in each of $C$ and $D$.
    b. The power method always converges to a finite, non-oscillatory value, but depending on the probability vector, the values are not always the same. A uniform vector gives the same result as a random walk, but other vectors give different results.
    c. Yes, the stochastic matrix for $\mathcal{G}_3$ is non-negative and irreducible, so the Perron--Frobenius theorem does apply.
4. &nbsp;
    a. $\mathcal{G}_4$ has two irreducible components: $\left\{A, B, C\right\}$ and $\{D, E, F\}$. The former is a source, and the latter is a sink.  
       $\mathcal{G}_5$ also has two irreducible components, both of which are sinks: $\left\{E\right\}$ and $\{F, G\}$.
    b. Random walks on $\mathcal{G}_4$ do always spends the same amount of time in each node. As one would expect for a sink and a source, the random walker spends very little time in the source before becoming trapped in the sink.  
       Random walks on $\mathcal{G}_5$ can get trapped in either $\{E\}$ or $\{F, G\}$, which means either spending all of the time on $E$, or splitting time evenly between $F$ and $G$, while spending almost no time on any of the rest of the nodes.
\setcounter{enumi}{5}
6. &nbsp;
    a. $H = \begin{bmatrix} 0 & 0 & \tfrac{1}{2} & \tfrac{1}{2} & 0 & 0 & 0\\
                            0 & 0 & \tfrac{1}{2} & \tfrac{1}{2} & 0 & 0 & 0\\
                            \tfrac{1}{2} & 0 & 0 & 0 & 0 & 0 & \tfrac{1}{2}\\
                            0 & \tfrac{1}{4} & 0 & 0 & \tfrac{1}{4} & \tfrac{1}{4} & \tfrac{1}{4}\\
                            0 & 0 & 0 & 0 & 0 & 0 & 0\\
                            0 & 0 & 0 & 0 & 0 & 0 & 0\\
                            0 & 0 & 0 & 0 & 0 & 0 & 0\end{bmatrix}$
        
        By definition, $\mathrm{\mathbf{d}} = \begin{bmatrix}0\\0\\0\\0\\1\\1\\1\end{bmatrix}$
        
        The goal is to produce a $\mathrm{\mathbf{w}}$ such that the elements for the three dangling nodes, $E$, $F$, and $G$, add to 1, and to close the network from nodes $E$, $F$, and $G$ to make the graph irreducible. The first criterion is satisfied by $\mathrm{\mathbf{w}} = \begin{bmatrix}0 & 0 & 0 & 0 & \tfrac{1}{3} & \tfrac{1}{3} & \tfrac{1}{3}\end{bmatrix}$, so when $\mathrm{\mathbf{d}}$ and $\mathrm{\mathbf{w}}$ are multiplied and added to $H$, the rows of $H$ for the dangling nodes will add to 1. To satisfy irreducibility, I will connect nodes $E$, $F$, and $G$ by adding a non-zero connection from each to node $A$.
        
        $\mathrm{\mathbf{w}} = \begin{bmatrix}0.1 & 0 & 0 & 0 & 0.3 & 0.3 & 0.3\end{bmatrix}$
        
        $S = H + \mathrm{\mathbf{d}}\mathrm{\mathbf{w}} = \begin{bmatrix} 0 & 0 & \tfrac{1}{2} & \tfrac{1}{2} & 0 & 0 & 0\\
                            0 & 0 & \tfrac{1}{2} & \tfrac{1}{2} & 0 & 0 & 0\\
                            \tfrac{1}{2} & 0 & 0 & 0 & 0 & 0 & \tfrac{1}{2}\\
                            0 & \tfrac{1}{4} & 0 & 0 & \tfrac{1}{4} & \tfrac{1}{4} & \tfrac{1}{4}\\
                            0.1 & 0 & 0 & 0 & 0.3 & 0.3 & 0.3\\
                            0.1 & 0 & 0 & 0 & 0.3 & 0.3 & 0.3\\
                            0.1 & 0 & 0 & 0 & 0.3 & 0.3 & 0.3\end{bmatrix}$
    b. Anything that doesn't make it irreducible or stochastic. An example would be the stochastic but nonirreducible vector $\mathrm{\mathbf{w}} = \begin{bmatrix}0 & 0 & 0 & 0 & \tfrac{1}{3} & \tfrac{1}{3} & \tfrac{1}{3}\end{bmatrix}$.
7. &nbsp;
    a. Even after 1,000,000 iterations, the proportions from the random walk did not appear to converge to more than 2 or 3 significant figures.
    b. Unlike in problem 3, which used a damping factor of $\alpha = 1$, with a damping factor of $\alpha = 0.85$, the power method does converge independent of the initial probability vector. $[0.3281 0.3163 0.1719 0.1837]$
8. &nbsp;
    a. Graph $\mathcal{G}_4$:
        i. Like $\mathcal{G}_3$, the random walk did not converge to more than 2 significant digits after a million iterations.
        ii. The power method converged to [0.1160 0.1070 0.05786 0.3617 0.1787 0.1787] independent of initial probability vector.
    b. Graph $\mathcal{G}_5$:
        i. The random walk did not converge to more than 2 significant digits after a million iterations.
        ii. The power method converged to [0.03727 0.03727 0.03727 0.03727 0.3540 0.2484 0.2484] independent of initial probability vector.
9. The algorithms do weight the nodes well for $\mathcal{G}_3$, and both values of $\alpha$ give fairly similar results.  
   For $\mathcal{G}_4$, the two values of $\alpha$ give wildly different results, but the results for $\alpha = 0.99$ seem reasonable.  
   The two $\alpha$s also give different results for $\mathcal{G}_5$, but in this case, the values when $\alpha = 0.85$ seem reasonable.
10. As we have seen throughout this lab, not all values of $\alpha$, $\mathrm{\mathbf{v}}$, and $\mathrm{\mathbf{v}}$ work equally well for graphs in general.

# Part II

1. The Google matrix of a graph is given by $G = \alpha S + (1-\alpha)1\mathrm{\mathbf{v}}$. In this case, $\alpha = 0.85$ and the stochastic matrix $S$ is  
    $S = \begin{bmatrix}0 & \tfrac{1}{3} & \tfrac{1}{3} & 0 & 0 & \tfrac{1}{3} & 0 & 0\\
                        0 & 0 & \tfrac{1}{2} & 0 & 0 & \tfrac{1}{2} & 0 & 0\\
                        0 & \tfrac{1}{3} & 0 & \tfrac{1}{3} & 0 & 0 & \tfrac{1}{3} & 0\\
                        \tfrac{1}{4} & 0 & 0 & 0 & \tfrac{1}{4} & \tfrac{1}{4} & 0 & \tfrac{1}{4}\\
                        0 & 0 & 0 & 0 & 0 & 1 & 0 & 0\\
                        \tfrac{1}{2} & 0 & 0 & 0 & \tfrac{1}{2} & 0 & 0 & 0\\
                        0 & 0 & 1 & 0 & 0 & 0 & 0 & 0\\
                        1 & 0 & 0 & 0 & 0 & 0 & 0 & 0\end{bmatrix}$
    
    $G$, therefore, is  
    $G = \alpha S + (1-0.85)\begin{bmatrix}1\\1\\1\\1\\1\\1\\1\\1\end{bmatrix}\begin{bmatrix}\tfrac{1}{8} & \tfrac{1}{8} & \tfrac{1}{8} & \tfrac{1}{8} & \tfrac{1}{8} & \tfrac{1}{8} & \tfrac{1}{8} & \tfrac{1}{8}\end{bmatrix}$
    
    So,  
    $G = \begin{bmatrix}0.01875 & 0.302083\bar{3} & 0.302083\bar{3} & 0.01875 & 0.01875 & 0.302083\bar{3} & 0.01875 & 0.01875\\
                        0.01875 & 0.01875 & 0.44375 & 0.01875 & 0.01875 & 0.44375 & 0.01875 & 0.01875\\
                        0.01875 & 0.302083\bar{3} & 0.01875 & 0.302083\bar{3} & 0.01875 & 0.01875 & 0.302083\bar{3} & 0.01875\\
                        0.23125 & 0.01875 & 0.01875 & 0.23125 & 0.01875 & 0.23125 & 0.23125 & 0.01875\\
                        0.01875 & 0.01875 & 0.01875 & 0.01875 & 0.01875 & 0.86875 & 0.01875 & 0.01875\\
                        0.44375 & 0.01875 & 0.01875 & 0.01875 & 0.44375 & 0.01875 & 0.01875 & 0.01875\\
                        0.01875 & 0.01875 & 0.86875 & 0.01875 & 0.01875 & 0.01875 & 0.01875 & 0.01875\\
                        0.86875 & 0.01875 & 0.01875 & 0.01875 & 0.01875 & 0.01875 & 0.01875 & 0.01875\end{bmatrix}$
    which is, in fact, stochastic.
    
    Using the power method, the ranking of the nodes is $F$, $C$, $A$, $E$, $B$, $G$, $D$, $H$. Weights are [0.1652 0.1141 0.1712 0.06726 0.1371 0.2449 0.06726 0.03304].
2. Modification I: The bad movies' ranking by the power method is $T$, $U$, $V$, $W$, with weights [0.3590 0.3426 0.1831 0.1153]. The combined network's rankings are $F$, $T$, $U$, $A$, $C$, $V$, $E$, $B$, $G$, $D$, $W$, $H$.

        [('F', 0.1551), ('T', 0.1167), ('U', 0.1117),
        ('A', 0.1051), ('C', 0.1003), ('V', 0.08795),
        ('E', 0.08710), ('B', 0.07070), ('G', 0.06584),
        ('D', 0.04092), ('W', 0.03742), ('H', 0.02120)]
    
    The bad movies are quite interspersed with the good movies, which seems strange because the bad movies have high centrality to each other, while only the $G \leftrightarrow V$ link connects the two graphs.

    In order to create the personalization vector for Guy Moviegoer, I first computed the sentiment of each of the movies using the naïve Bayes classifier in the NLTK ([code](https://bitbucket.org/Noleli/nlp-lab-3/src/63113e0dfd9b/classifymovies.py)). I then normalized the rating scores to make it a stochastic vector and inverted each element to weight the badly rated movies more highly.

    $$v_i = -1 \frac{r_i}{\sum{r}}$$

    Using the power method with $\mathbf{\mathrm{v}}$ as the personalization vector, the PageRank scores are:

        [('U', 0.21833109331028794), ('T', 0.2129461733532912),
        ('V', 0.09859531983529451), ('W', 0.06458177853690886),
        ('H', -0.059018631011186304), ('G', -0.07414399603764962),
        ('D', -0.10207933665764973), ('B', -0.18370490866244582),
        ('C', -0.22853820246232703), ('E', -0.23792441777506612),
        ('A', -0.28809025413457445), ('F', -0.42095461829488273)]

    (To avoid the negatives, it might have been better to add one and subtract from two rather than just multiplying by negative one.)

3. Modification II:
    
    Adding the bad movies to the matrix does not affect the relative rankings of the good movies ($F$, $C$, $A$, $E$, $B$, $G$, $D$, $H$).

    To push down the recommendation weighting of movies that have outlinks pointing to poorly rated movies, I created a personalization vector based on the following rule:

    For each node, compute the average of the sentiment scores of each of the linked-to movies. Then average that with the sentiment score of the current movie, effectively giving the score of the movie and the score of the linked-to movies equal weight.

        connected_node_average_score = {}
        for n in modtwo:
            thesum = 0
            for j in modtwo[n]:
                thesum += scores[j]
            avg = thesum/len(modtwo[n])

            # weight the given movie as half. seems reasonable
            connected_node_average_score[n] = (avg + scores[n])/2

    Then add 1 to each score to make them all positive and normalize them to make the stochastic personalization vector.

        plusone = {n: connected_node_average_score[n] + 1 for n in
            connected_node_average_score}
        thesum = sum(plusone.values())
        personalization = {n: plusone[n]/thesum for n in plusone}

