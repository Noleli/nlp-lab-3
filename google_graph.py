# This file contains some graphs represented as dictionaries.
# See following website for more information on dictionaries 
# http://docs.python.org/tut/node7.html#SECTION007500000000000000000 

# Dictionaries can be thought of as maps. Our graphs are stored as maps from 
# nodes to lists of nodes. If g is our graph and 'A' is a node, then g['A'] is the list of
# nodes which can be reached from an outgoing edge from 'A'.

import random
# random number routines

class google_graph(dict):

    def number_of_nodes(self):
        'Return the number of nodes'
        return len(self)

    def number_of_edges(self):
        'Return the number of edges'
        return sum([len(out_neighbors) for out_neighbors in self.values()])
    
    def random_node(self, weight_vector=None):
        'Returns a random node from graph, optionally weighted by weight_vector'
        # default to uniform probability    
        if (weight_vector == None):
            return random.choice(self.keys())
        # but if weight vector is supplied,
        #   select element from cumulative probability density function of weight_vector
        x = sum([weight_vector[node] for node in weight_vector]) * random.random()   # random number 0 <= x < 1
        cum_weight = 0.0
        for node in self.keys():
            cum_weight += weight_vector[node]
            if (cum_weight > x): return node

    def random_edge(self, from_node):
        'Returns a random neighbor of from-node'
        return random.choice(self[from_node])

    def vector(self, value = None):
        'Returns a node-indexed vector with all entries set to value'
        'Defaults to uniform probability weighting on each node'
        if value != None: weight = value
        else: weight=1.0/len(self)
        return dict([(node, weight) for node in self])

    def multiply_vector(self, vector, alpha = 1.0, personalization = None, dangling = None):
        'Returns new_vector = alpha*S(vector) + (1-alpha)v'
        '   where S = G + dw implements the dangling node fix according to w'
        ''
        '   alpha:      damping factor (defaults to 1)'
        '   v:          personalization vector (defaults to uniform vector)'
        '   w:          dangling node fix  (defaults to uniform vector)'

        if personalization: v = personalization
        else:               v = self.vector()
        if dangling:        w = dangling
        else:               w = self.vector()

        new_vector = {}
        
        # First do the (1-alpha)v part        
        for node in v.keys():
            new_vector[node] = (1-alpha)*v[node]
            
        # Now do the alpha*S(vector) part
        for node in self.keys():                    # For each node in the graph
            out_neighbors = self[node]
            if out_neighbors:                           # If there are outgoing edges
                for adjacent_node in self[node]:            # Spread the probability evenly among them
                    new_vector[adjacent_node] += alpha*vector[node]/len(out_neighbors)
            else:                                       # If not, spread it according to w
                for adjacent_node in w.keys():          #   (dangling node fix)
                    new_vector[adjacent_node] += alpha*w[adjacent_node]*vector[node]

        return new_vector

    def random_walk(self, length, start_node = None, alpha = 1.0, v = None, w = None):
        'Performs a random walk and returns a probability vector'
        '   indicating the visitation distribution'
        ''
        '   start_node: first node of the walk (defaults to a random node)'
        '   alpha:      damping factor (defaults to 1)'
        '   v:          personalization vector (defaults to uniform vector)'
        '   w:          dangling node fix  (defaults to uniform vector)'

        # start at start_node, default to random selection
        if start_node == None: start_node = self.random_node()

        # MAIN LOOP: PSEUDOCODE
        #   with alpha probability, walk along a random outgoing edge
        #       (or if there is no outgoing edge select a random destination)
        #   with 1-alpha probability, go to a node according to the personalization vector

        # node tracks current location
        # walk_vector counts number of times each location has been visited
        node, walk_vector = start_node, self.vector(0)
        for i in xrange(length):
            if (random.random()<alpha):
                try:                node = self.random_edge(node)
                except IndexError:  node = self.random_node(w)
            else:
                node = self.random_node(v)
            walk_vector[node] += 1
            
        # Now scale the vector down by the number of vertices visited
        for node in self.keys():
            walk_vector[node] = float(walk_vector[node])/length
        return walk_vector

def sort_vector_by_weight(vector):
    'Return list of (node, weight) pairs in order of decreasing weight'
    # Generate list of (weight, node) tuples
    backitems = [(v[1], v[0]) for v in vector.items()]
    # Sort (default sort for tuples is lexicographic, i.e. by weight)
    backitems.sort()
    backitems.reverse()
    return [(i[1], i[0]) for i in backitems]

def Linf(x, y):
    'Return the maximum difference between the entries'
    return max([abs(x[node] - y[node]) for node in x])


