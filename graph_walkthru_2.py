
#
# More hints for using the "google_graph" class in "google_graph.py"
# This time we will look at the dangling node fix, personalization vector, and damping factor.

from google_graph import *

def pretty(vector, header = True):
    if (isinstance(vector, dict)):
	keys = vector.keys()
	keys.sort()
	if header: pretty_string = '\t'.join([str(key) for key in keys]) + '\n'
	else: pretty_string = ''
	pretty_string += '\t'.join(['%.3f' %vector[key] for key in keys])
	return(pretty_string)
    if  (isinstance(vector,list)):
	if header: pretty_string = '\t'.join([str(key[0]) for key in vector]) + '\n'
	else: pretty_string = ''
	pretty_string += '\t'.join(['%.3f' %key[1] for key in vector])
	return(pretty_string)
	

def wait_for_user(no_request = False):
    if no_request: request_string = ''
    else: request_string = '\nPress enter to continue... '
    try: input(request_string)
    except SyntaxError: pass

# Import a g6 from the module graphs.py
from graphs import g6

print "*** Experimenting with dangling nodes"

print "\nThe following is the graph g6. Note that E, F, and G are dangling nodes."
print g6
print "The graph g6 has %(nodes)i nodes and %(edges)i edges" % \
	{'nodes':g6.number_of_nodes(), 'edges':g6.number_of_edges()}
# The site http://docs.python.org/lib/typesseq-strings.html describes how to format strings as above.
wait_for_user()


print "\nHere are the results of a random walk of length 100 on g6 with no damping and a dangling node fix given by the uniform vector."
print pretty(sort_vector_by_weight(g6.random_walk(100)))
wait_for_user()

print "\nHere are the results of multiplying the uniform vector 100 times by the google matrix of g6 with no damping and a dangling node fix given by the uniform vector."
v=g6.vector()
for i in xrange(100):
	v=g6.multiply_vector(v,1.0)
print pretty(sort_vector_by_weight(v))
wait_for_user()

print "\nLet w be the vector which gives all weight to node A"
w=g6.vector(0)
w['A']=1.0
print pretty(w)
wait_for_user()

print "\nHere are the results of a random walk of length 100 on g6 with no damping and a dangling node fix given by w."
print pretty(sort_vector_by_weight(g6.random_walk(100,None,1.0,None,w)))
wait_for_user()

print "\nHere are the results of multiplying the uniform vector 100 times by the google matrix of g6 with no damping and a dangling node fix given by w."
v=g6.vector()
for i in xrange(100):
	v=g6.multiply_vector(v,1.0,None,w)
print pretty(sort_vector_by_weight(v))
wait_for_user()

print "\n*** Experimenting with the damping factor and personalization vector"

from graphs import g2
print "\nThe graph g2 is "
print g2
wait_for_user()

print "\nHere are the results of a random walk of length 1000 from the node 'A' on g2 with a damping factor of 0.85 and a uniform personalization vector"
print pretty(sort_vector_by_weight(g2.random_walk(1000,'A',0.85)))
wait_for_user()

print "\nHere are the results with a damping factor of 0.1"
print pretty(sort_vector_by_weight(g2.random_walk(1000,'A',0.1)))
print "The results were much more uniform."
wait_for_user()

print "\nHere are the results of a repeated multiplication of the uniform vector by the google matrix, with a damping factor of 0.85 and the uniform personalization factor"
v=g2.vector()
for i in xrange(100):
	v=g2.multiply_vector(v,0.85)
print pretty(sort_vector_by_weight(v))
wait_for_user()

print "\nHere are the results with a damping factor of 0.1"
v=g2.vector()
for i in xrange(100):
	v=g2.multiply_vector(v,0.1)
print pretty(sort_vector_by_weight(v))
print "The results were again much more uniform."
wait_for_user()



print "\nHere is a personalization vector which gives all weight to the node E"
personalization_vector=g2.vector(0.0)
personalization_vector['E']=1.0
print pretty(sort_vector_by_weight(personalization_vector))
wait_for_user()

print "\nHere are the results of a random walk with this personalization vector and a damping factor of 0.85"
print pretty(sort_vector_by_weight(g2.random_walk(100,None,0.85,personalization_vector)))
wait_for_user()

print "\nHere are the results of repeated multiplication of the uniform vector by the google matrix with the same personalization vector and a damping factor of .95"
v=g2.vector()
for i in xrange(100):
	v=g2.multiply_vector(v,0.95,personalization_vector)
print pretty(sort_vector_by_weight(v))
print "This seems more fair to the other nodes!"
wait_for_user()


print "\n*** Experimenting convergence rates"

# How long does it take the power method to converge for g2.
# 100 iterations should give the eigenvector
v=g2.vector()
for i in xrange(100):
	v=g2.multiply_vector(v,0.85)
print "\nLet G be the google matrix and v to be the uniform vector. The following is v*G^100."
print pretty(sort_vector_by_weight(v))
wait_for_user()

v2=g2.multiply_vector(v,0.85)
print "\nThe following is v*G^101."
print pretty(sort_vector_by_weight(v2))
print "The entries are all within %(distance)g of each other, so this is pretty close to an eigenvector." % {'distance': Linf(v,v2) }
wait_for_user()

# We will see how fast the uniform vector converges to 
u=g2.vector()
i=0
while (Linf(u,v)>.00001):
	i=i+1
	u=g2.multiply_vector(u,0.85)
print "\nLet v be the uniform vector. Each entry of the vector v*G^%(iterations)i is within 0.00001 of the eigenvector." % {'iterations':i}

