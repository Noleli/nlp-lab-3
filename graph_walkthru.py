
# Hints for using the functions defined in "graph_algorithms.py"

from google_graph import *  # access this module by prefixing with "google_graph."
from graphs import *        # access this module directly, without prefixing

def pretty(vector, header = True):
    if (isinstance(vector, dict)):
	keys = vector.keys()
	keys.sort()
	if header: pretty_string = '\t'.join([str(key) for key in keys]) + '\n'
	else: pretty_string = ''
	pretty_string += '\t'.join(['%.3f' %vector[key] for key in keys])
	return(pretty_string)
    if  (isinstance(vector,list)):
	if header: pretty_string = '\t'.join([str(key[0]) for key in vector]) + '\n'
	else: pretty_string = ''
	pretty_string += '\t'.join(['%.3f' %key[1] for key in vector])
	return(pretty_string)
	

def wait_for_user(no_request = False):
    if no_request: request_string = ''
    else: request_string = '\nPress enter to continue... '
    try: input(request_string)
    except SyntaxError: pass

print 'The graph G_2 in python is '
print g2
wait_for_user()

print '\nThe following is a vector representing portion of time a random'
print 'walk of length 100 spends at each node in the graph.'
v = g2.random_walk(100)
print pretty(v)
wait_for_user()

print '\nThe following is the above vector sorted by node weight'
print pretty(sort_vector_by_weight(v))
wait_for_user()

print '\nHere is the uniform vector associated to G_2.'
print pretty(g2.vector())
wait_for_user()

print '\nHere is a vector, v, which weights vertices A and B'
print 'but doesn\'t assign weight to other vertices.'
v = g2.vector(0.)
v['A']=0.6
v['B']=0.4
print pretty(v)

print '\nHere is the product v*P, where P is the stochastic matrix'
print 'associated to g2.'
print pretty(g2.multiply_vector(v))
wait_for_user()

print '\nHere is the product v*P^100.'
for i in xrange(100):
    v = g2.multiply_vector(v)
print pretty(v)
wait_for_user()

print '\nAnd here is the same product, sorted by weight.'
print pretty(sort_vector_by_weight(v))
wait_for_user()

print '\nHere is what we get when we multiply once again'
print pretty(sort_vector_by_weight(g2.multiply_vector(v)))
wait_for_user()

print '\nBecause the ranking changed, the system must not be close to'
print 'convergence. Take a closer look at the exact values:'
print pretty(v)
print pretty(g2.multiply_vector(v), header=False)
print '\nDo you notice any similarities between the two distributions?'
wait_for_user()

print '\nNow let\'s put a pulse into node A and watch it move.'
print 'In the table below, time is implicitly represented by rows.'
print 'The first row indicates the initial state. Press enter'
print 'after each row to see the next iteration. It will terminate'
print 'after 10 iterations.'
v = g2.vector(0.)
v['A'] = 1.0
print pretty(v)
for i in xrange(10):
    v = g2.multiply_vector(v)
    wait_for_user(no_request = True)
    print pretty(v, header=False)
print '\nWhen does the pulse actually mix with other pulses, and'
print 'when is it just looping through the system without mixing?'
    
##    v = g2.multiply_vector(v)
##    print sort_vector_by_weight(v)

