from google_graph import google_graph

g1 = google_graph({'A': ['B'],
	# in the following line 'B' is a node name, and ['A', 'C'] is the list of nodes which can be reached from edges leaving 'B'.
	'B': ['A', 'C'],
	'C': ['A']})
g2 = google_graph({'A': ['B'],
	'B': ['C'],
	'C': ['D'],
	'D': ['A','E'],
	'E': ['F'],
	'F': ['G'],
	'G': ['H'],
	'H': ['I'],
	'I': ['A']})
g3 = google_graph({'A': ['B'],
	'B': ['A','C'],
	'C': ['D'],
	'D': ['A']})
g4 = google_graph({'A':['B','C', 'D'],
	'B':['A'],
	'C':['B'],
	'D':['E','F'],
	'E':['D'],
	'F':['D']})
g5 = google_graph({'A':['B','E'],
	'B':['C','F'],
	'C':['D','G'],
	'D':['A','E'],
	'E':['E'],
	'F':['G'],
	'G':['F']})
g6 = google_graph({'A':['B','C'],
	'B':['C','D'],
	'C':['A','G'],
	'D':['B','E','F','G'],
	'E':[],
	'F':[],
	'G':[]})

# Artificial websites from part 4 of the lab.
# Site 1: (Organized hierarchical website?)
site1 = google_graph({'A': ['B','C'],
	'B':['A','D','E'],
	'C':['A','F'],
	'D':['A','B'],
	'E':['A','B','G'],
	'F':['A','C'],
	'G':['A','B','E']})
# Site 2: (A less organized sparsely connected website?)
site2 = google_graph({'A':['B'],
	'B':['C','E'],
	'C':['A','D'],
	'D':['G'],
	'E':['F'],
	'F':['B'],
	'G':['C']})

# Part II
movies = google_graph({'A': ['B', 'C', 'F'],
    'B': ['C', 'F'],
    'C': ['B', 'D', 'G'],
    'D': ['A', 'E', 'F', 'H'],
    'E': ['F'],
    'F': ['A', 'E'],
    'G': ['C'],
    'H': ['A']})

badmovies = google_graph({'T': ['U'],
    'U': ['V', 'T'],
    'V': ['W', 'T'],
    'W': ['T']})

goodbad = google_graph({'A': ['B', 'C', 'F'],
    'B': ['C', 'F'],
    'C': ['B', 'D', 'G'],
    'D': ['A', 'E', 'F', 'H'],
    'E': ['F'],
    'F': ['A', 'E'],
    'G': ['C', 'V'],
    'H': ['A'],
    'T': ['U'],
    'U': ['V', 'T'],
    'V': ['W', 'T', 'G'],
    'W': ['T']})

modtwo = google_graph({'A': ['B', 'C', 'F'],
    'B': ['C', 'F'],
    'C': ['B', 'D', 'G'],
    'D': ['A', 'E', 'F', 'H'],
    'E': ['F'],
    'F': ['A', 'E'],
    'G': ['C'],
    'H': ['A'],
    'T': ['G', 'U', 'V'],
    'U': ['B', 'G', 'V'],
    'V': ['D', 'G'],
    'W': ['A', 'H']})