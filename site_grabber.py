from urllib import urlopen
from urlparse import urlparse
from HTMLParser import HTMLParser
from HTMLParser import HTMLParseError
from urlparse import urljoin
import sys

class MyParser(HTMLParser):
	" class"
	def __init__(self, url,root=None):
		"init"	
		print "Processing "+url
		HTMLParser.__init__(self)
		self.url=url
		if (root==None): self.root=url
		else: self.root=root
		self.list=[]
		f = urlopen(self.url)
		s = f.read()
		# HTMLParser does not seem to handle newlines in tags... so we do a hack
		# split into individual lines
		lines = s.splitlines()
		# reassemble
		self.s = ""
		for i in range(0,len(lines)):
			self.s=self.s+" "+lines[i]	
		err = 1
		while err == 1:
			try:
				self.feed(self.s)
				err = 0
				break
			except HTMLParseError:
				self.s=self.s[self.offset:-1]
				self.reset();
				err = 1
	
	def add(self,url) :
		self.list.append(url)
	
	def handle_starttag(self, tag, attrs):
		if (tag=="a"): 
			for name, value in attrs:
				if name == "href":
					# strip any whitespace from the url (This is improper syntax that was throwing off the program)
					value=value.strip();
					newurl=urljoin(self.url, value)
					if not newurl.startswith(self.root):
						# throw out any url which would leave the site
						#print "throw out "+newurl
						return
					# split new url into pieces
					newurl=urlparse(newurl)
					# clear out some parts of the url we don't care about
					newurl=newurl[0]+"://"+newurl[1]+newurl[2]					
					# strip duplicate names
					if newurl.endswith("/index.html"):
						newurl=newurl[0:-10]
					if newurl.endswith("/index.php"):
						newurl=newurl[0:-9]
					dot=newurl.rfind(".")
					slash=newurl.rfind("/")
					if (dot>slash) :
						extension=newurl[dot:]
						if (extension==".html") or (extension==".htm") or (extension==".php"):
							self.add(newurl)
						#else:
							#print "discarding "+newurl
					else:
						if (newurl[-1]!="/"): self.add(newurl+"/")
						else: self.add(newurl)
						
					
					
class GraphBuilder:
	def __init__(self, start, root=None):
		self.start=start
		if (root==None): self.root=start
		else: self.root=root
		self.todo=[start]
		self.graph={}
		while len(self.todo):
			url=self.todo.pop()
			if (not self.graph.has_key(url)):
				myparser = MyParser(url,self.root)
				self.graph[url]=myparser.list
				self.todo.extend(myparser.list)


# Return a new graph, with the first n characters removed from each node name
def crop(graph,n):
	new_graph={}
	for node in graph.keys():
		adjacent_nodes=[]
		for adj in graph[node]:
			adjacent_nodes.append(adj[n:])
		new_graph[node[n:]]=adjacent_nodes
	return new_graph

#~ # The following code grabs the graph from Janet's website and pickles it.
#~ g=GraphBuilder("http://faculty.wcas.northwestern.edu/~jbp/")
#~ # remove the common beginning of node names
#~ graph=crop(g.graph,len("http://faculty.wcas.northwestern.edu/~jbp"))
#~ file = open("jbp.dat","w")
#~ import pickle
#~ pickle.Pickler(file).dump(graph)

#~ # The following code loads the graph from the jbp.dat file
#~ file = open("jbp.dat","r")
#~ import pickle
#~ g=pickle.Unpickler(file).load()
#~ print g

# The following code grabs the graph from the Northwestern summer session and pickles it.
g=GraphBuilder("http://www.scs.northwestern.edu/summernu/")
# remove the common beginning of node names
graph=crop(g.graph,len("http://www.scs.northwestern.edu/summernu"))
file = open("summernu.dat","w")
import pickle
pickle.Pickler(file).dump(graph)

