from google_graph import *  # access this module by prefixing with "google_graph."
from graphs import *        # access this module directly, without prefixing
import classifymovies

# 1a
# stocmatrix = {}
# for node in g1:
#     stocmatrix[node] = []
#     for i, link in enumerate(g1[node]):
#         stocmatrix[node].append((link, 1/(float(len(g1[node])))))
# 
# # {'A': [('B', 1.0)], 'C': [('A', 1.0)], 'B': [('A', 0.5), ('C', 0.5)]}
# 
# g1smatrix = google_graph(stocmatrix)
# 
# # 1b
# v = g1.vector()
# for i in range(1001):
#     v = g1.multiply_vector(v)
# 
# # 1c
# g1.random_walk(10000)
# converges to [0.4, 0.4, 0.2], which makes sense because A has two inlinks and can only go to B, so a random walk will spend the same amount of time on each of those, while B can either go back to A or go on to C, so it only spends half the time on C as B.

# 2a
# v1 = g2.vector()
# v2 = g2.vector()
# v2['A'] = v2['A'] - .1
# v2['D'] = v2['D'] + .1
# v3 = g2.vector(0.0)
# v3['B'] = 0.4
# v3['E'] = 0.3
# v3['H'] = 0.3
# 
# 
# for i in range(1001):
#     v1 = g2.multiply_vector(v1)
#     v2 = g2.multiply_vector(v2)
#     v3 = g2.multiply_vector(v3)
# 
# v1sorted = []
# for key in sorted(v1.iterkeys()):
#     v1sorted.append(v1[key])
# 
# # 3a
# w = g3.random_walk(10000)
# wsorted = []
# for key in sorted(w.iterkeys()):
#     wsorted.append(w[key])

# 3b
# v1 = g5.vector()
# v2 = g5.vector()
# v2['A'] = v2['A'] - .1
# v2['D'] = v2['D'] + .1
# v3 = g5.vector(0.0)
# v3['A'] = 0.4
# v3['B'] = 0.3
# v3['C'] = 0.3
# 
# for i in range(1001):
#     v1 = g5.multiply_vector(v1, 0.85)
#     v2 = g5.multiply_vector(v2, 0.85)
#     v3 = g5.multiply_vector(v3, 0.85)
# 
# v1sorted = []
# for key in sorted(v1.iterkeys()):
#     v1sorted.append(v1[key])

# 4c


# Part 2
# 6a
# w=g6.vector(0)
# w['E'] = 1.0
# w['F'] = 1.0
# w['G'] = 1.0
# H = g6.random_walk(1000)
# 
# H = {}
# for node in g6:
#     H[node] = []
#     for i, link in enumerate(g6[node]):
#         H[node].append((link, 1/(float(len(g6[node])))))

# length, start_node = None, alpha = 1.0, v = None, w = None
# w = g6.random_walk(10000,None,1.0,None,w)
# wsorted = []
# for key in sorted(w.iterkeys()):
#     wsorted.append(w[key])
# print pretty(sort_vector_by_weight(g6.random_walk(100,None,1.0,None,w)))

# 9
def power_method(graph, alpha, personalization = None):
    v = graph.vector()
    for i in range(10001):
        v = graph.multiply_vector(v, alpha, personalization)
    return v

# print "G3, 0.85: " + str(power_method(g3, 0.85))
# print "G3, 0.99: " + str(power_method(g3, 0.99))
# print "G4, 0.85: " + str(power_method(g4, 0.85))
# print "G4, 0.99: " + str(power_method(g4, 0.99))
# print "G5, 0.85: " + str(power_method(g5, 0.85))
# print "G5, 0.99: " + str(power_method(g6, 0.99))

# Part II
# v = power_method(movies, 0.85)
# print sort_vector_by_weight(v)
# 
# b = power_method(badmovies, 0.85)
# print sort_vector_by_weight(b)
# 
gb = power_method(goodbad, 0.85)
print sort_vector_by_weight(gb)

# Mod 1
scores = {}
files = ["A", "B", "C", "D", "E", "F", "G", "H", "T", "U", "V", "W"]
# for letter in files:
#     scores[letter] = classifymovies.scoreMovie(letter)
# scoresfile = open("scores.txt", "w")
# scoresfile.write(str(scores))
# scoresfile.close()

# Hardcode the results for speed
scores = {'A': 1.0, 'C': 1.0, 'B': 1.0, 'E': 1.0, 'D': 1.0, 'G': 1.0, 'F': 1.0, 'H': 0.9999980178192729, 'U': -1.0, 'T': -0.9999667895876992, 'W': -0.9817716170337306, 'V': -0.9997041682659441}

# probably want to take these scores, normalize them, invert them to favor bad movies, and use that as the personalization vector.
thesum = sum(scores.values())
invnormscores = {x: -1*scores[x]/thesum for x in scores}

weighting = power_method(goodbad, 0.85, invnormscores);
print sort_vector_by_weight(weighting)

# Mod 2

connected_node_average_score = {}
for n in modtwo:
	thesum = 0
	for j in modtwo[n]:
		thesum += scores[j]
	avg = thesum/len(modtwo[n])

	# weight the given movie as half. seems reasonable
	connected_node_average_score[n] = (avg + scores[n])/2

plusone = {n: connected_node_average_score[n] + 1 for n in connected_node_average_score}
thesum = sum(plusone.values())
personalization = {n: plusone[n]/thesum for n in plusone}
print personalization